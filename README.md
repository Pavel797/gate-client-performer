# Разворачивание на устройстве:

 Для orangepi zero:

Скачать и записать образ (https://yadi.sk/d/19MI0GzGbOPH9g) на флешку 

> apt-get update

> apt-get upgrade 

> apt-get install python3-dev

> git clone https://github.com/nvl1109/orangepi_zero_gpio.git

> cd orangepi_zero_gpio

> python setup.py install

> nano /lib/systemd/system/gate-client-performer.service

или 

> nano /etc/systemd/system/gate-client-performer.service


[Unit]
Description=Gate-client-performer Service
After=multi-user.target
Conflicts=getty@tty1.service
[Service]
Restart=always
Type=simple
ExecStart=/usr/bin/python3 #{path to main.py}
StandardInput=tty-force
[Install]
WantedBy=multi-user.target


> systemctl daemon-reload

> systemctl enable gate-client-performer.service
> systemctl start  gate-client-performer.service


> systemctl status gate-client-performer.service


> systemctl stop gate-client-performer.service          #Для остановки сервиса 
> systemctl start gate-client-performer.service         #Для запуска сервиса
> systemctl restart gate-client-performer.service       #Для перезапуска сервиса