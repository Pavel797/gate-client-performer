class port:
    """
    Заглушка, необходима из-за того, что на машине разработчика нету физического светодиода
    """
    STATUS_LED = None


class gpio:
    """
    Заглушка, необходима из-за того, что на машине разработчика нету физического светодиода
    """
    PULLUP = None
    INPUT = None
    OUTPUT = None

    @classmethod
    def init(cls):
        pass

    @classmethod
    def setcfg(cls, led, OUTPUT):
        pass

    @classmethod
    def output(cls, led, param):
        pass

    @classmethod
    def pullup(cls, PORT_OPEN_BTN, PULLUP):
        pass

    @classmethod
    def input(cls, PORT_OPEN_BTN):
        return 1
