import os
import sys

import time
import websocket
import asyncio
import logging

from queue import Queue
from threading import Thread

try:
    from pyA20.gpio import gpio
    from pyA20.gpio import port
except ImportError:
    from utils import gpio, port

PORT_LED_OPEN = 7
PORT_LED_STATUS = 2
PORT_OPEN_BTN = 19
PORT_OPEN_TRIGGER = 18

HOST_NAME = '68.183.64.161'
SECRET_KEY = 'sdfdfsdfsdfs'
OPEN_TIME_OUT = 15

logging.basicConfig(filename="logfile.log", level=logging.INFO, format='[%(asctime)s] : %(message)s')


class BlinkLedThread(Thread):
    BLINK_TYPE_CONNECT = 1
    BLINK_TYPE_CONNECTING = 2
    BLINK_TYPE_PING = 3
    BLINK_TYPE_OFF = 4

    __LONG_BLINK = 1.0
    __SHORT_BLINK = 0.2

    def __init__(self, queue):
        super().__init__()
        self.__queue = queue
        self.__is_stop = False
        self.__current_state = BlinkLedThread.BLINK_TYPE_OFF

        self.start()

    def __long_blink(self, led_port):
        self.__blink(self.__LONG_BLINK, led_port)

    def __short_blink(self, led_port):
        self.__blink(self.__SHORT_BLINK, led_port)

    def __blink(self, delay, led_port):
        gpio.output(led_port, 1)
        time.sleep(delay)
        gpio.output(led_port, 0)
        time.sleep(delay)

    def run(self):
        while not self.__is_stop:
            time.sleep(0.3)
            # print('run', self.__current_state)
            if self.__queue.qsize():
                state = self.__queue.get()
                if state == BlinkLedThread.BLINK_TYPE_CONNECTING:
                    self.__current_state = state
                else:
                    if state == BlinkLedThread.BLINK_TYPE_CONNECTING:
                        self.__long_blink(PORT_LED_STATUS)
                        self.__long_blink(PORT_LED_STATUS)
                        self.__long_blink(PORT_LED_STATUS)
                    elif state == BlinkLedThread.BLINK_TYPE_PING:
                        self.__long_blink(PORT_LED_STATUS)

                    self.__current_state = BlinkLedThread.BLINK_TYPE_OFF

            if self.__current_state == BlinkLedThread.BLINK_TYPE_CONNECTING:
                self.__short_blink(PORT_LED_STATUS)

    def stop(self):
        self.__is_stop = True


class WebSocketPingThread(Thread):
    URL_WS_TEMPLATE = 'ws://{host_name}/ws/v1/smartgate/ping-pong/clientperformer/{secret_key}/'

    def __init__(self, host_name, secret_key, queue_blink_led):
        self.URL_WS = WebSocketPingThread.URL_WS_TEMPLATE.format(secret_key=secret_key, host_name=host_name)
        self.queue_blink_led = queue_blink_led
        # websocket.enableTrace(True)
        super().__init__()
        self.ws = websocket.WebSocketApp(self.URL_WS,
                                         on_message=lambda ws, message: self.on_message(message),
                                         on_error=lambda ws, error: self.on_error(error),
                                         on_close=lambda ws: self.on_close(),
                                         on_open=lambda ws: self.on_open())

    def run(self):
        self.ws.run_forever()

    def on_message(self, message):
        print('message', message)
        if message == 'ping':
            self.queue_blink_led.put(BlinkLedThread.BLINK_TYPE_PING)
            self.ws.send("pong")

    def on_error(self, error):
        print('error', error)
        self.ws.close()
        self.start()

    def on_close(self):
        print("### closed ###")
        time.sleep(2)
        self.start()

    def on_open(self):
        print("on_open")


class OpeningThread(Thread):
    OPEN = 1

    def __init__(self, queue):
        super().__init__()
        self.__queue = queue
        self.is_stop = False

        self.__time_open = -1

        self.start()

    def __open(self):
        self.__time_open = time.time()
        gpio.output(PORT_LED_OPEN, 1)

        gpio.output(PORT_OPEN_TRIGGER, 1)
        time.sleep(2)
        gpio.output(PORT_OPEN_TRIGGER, 0)

        while self.__queue.qsize():
            self.__queue.get()

    def run(self):
        while not self.is_stop:
            time.sleep(0.2)
            state = gpio.input(PORT_OPEN_BTN)
            state1 = gpio.input(PORT_OPEN_BTN)
            state2 = gpio.input(PORT_OPEN_BTN)

            if not state and not state1 and not state2 and self.__time_open == -1:
                print('open btn')
                self.__open()

            if self.__time_open > 0 and (time.time() - self.__time_open) > OPEN_TIME_OUT:
                gpio.output(PORT_LED_OPEN, 0)
                self.__time_open = -1

            if self.__queue.qsize():
                msg = self.__queue.get()
                if msg == OpeningThread.OPEN and self.__time_open == -1:
                    self.__open()

    def stop(self):
        self.is_stop = True


class GateClient:
    URL_WS_OPEN = 'ws://{host_name}/ws/v1/smartgate/client-performer/{secret_key}/'.format(
        host_name=HOST_NAME, secret_key=SECRET_KEY)

    def __init__(self):
        gpio.init()
        gpio.setcfg(PORT_LED_OPEN, gpio.OUTPUT)
        gpio.setcfg(PORT_LED_STATUS, gpio.OUTPUT)
        gpio.setcfg(PORT_OPEN_TRIGGER, gpio.OUTPUT)
        gpio.setcfg(PORT_OPEN_BTN, gpio.INPUT)

        gpio.pullup(PORT_OPEN_BTN, gpio.PULLUP)

        self.queue_blink_led = Queue()
        self.blink_connect = BlinkLedThread(self.queue_blink_led)

        self.queue_open = Queue()
        self.open_thread = OpeningThread(self.queue_open)

        # websocket.enableTrace(True)
        self.ws = websocket.WebSocketApp(GateClient.URL_WS_OPEN,
                                         on_message=lambda ws, message: self.on_message(ws, message),
                                         on_error=lambda ws, error: self.on_error(ws, error),
                                         on_close=lambda ws: self.on_close(ws),
                                         on_open=lambda ws: self.on_open(ws))

        self.wst_ping = WebSocketPingThread(HOST_NAME, SECRET_KEY, self.queue_blink_led)

    def start(self):
        logging.info("start")
        self.queue_blink_led.put(BlinkLedThread.BLINK_TYPE_CONNECTING)
        self.wst_ping.start()
        self.ws.run_forever()

    def stop(self):
        logging.info("stop")
        self.blink_connect.stop()
        self.open_thread.stop()
        self.ws.close()
        self.wst_ping.ws.close()

    def on_message(self, ws, message):
        print('message', message, time.time())
        if message == 'open':
            ws.send("ok")
            self.queue_open.put(OpeningThread.OPEN)

    def on_error(self, ws, error):
        logging.error("error: {}".format(error))
        print('error', error)
        self.ws.close()
        self.start()

    def on_close(self, ws):
        print("### closed ###")
        logging.info("closed")
        time.sleep(2)
        logging.info("restart")
        self.start()

    def on_open(self, ws):
        print("on_open")
        logging.info("on_open")
        self.queue_blink_led.put(BlinkLedThread.BLINK_TYPE_CONNECT)


# cat /sys/devices/virtual/thermal/thermal_zone0/temp
# cat /sys/devices/virtual/thermal/thermal_zone1/temp

def main():
    client = GateClient()
    try:
        client.start()
    except KeyboardInterrupt:
        print("\nExit")
    finally:
        client.stop()

        gpio.output(PORT_LED_OPEN, 0)
        gpio.output(PORT_LED_STATUS, 0)
        gpio.output(PORT_OPEN_TRIGGER, 0)


if __name__ == '__main__':
    # if not os.getegid() == 0:
    #    sys.exit('The script must be run by administrator.')

    main()
